package com.xolidosoft.overflow;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.xolidosoft.overflow.entities.Usuario;
import com.xolidosoft.overflow.helpers.UsuarioDatabaseHelper;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_MAIL = "EXTRA_MAIL";
    public static final String EXTRA_PASS = "EXTRA_PASS";

    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        usuario = new Usuario(Integer.parseInt(intent.getStringExtra(LoginActivity.EXTRA_ID)),intent.getStringExtra(LoginActivity.EXTRA_NAME),intent.getStringExtra(LoginActivity.EXTRA_MAIL),intent.getStringExtra(LoginActivity.EXTRA_PASS));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView tv_username = (TextView) headerView.findViewById(R.id.textView_user_name);
        tv_username.setText(usuario.getNombre());
        TextView tv_usermail = (TextView) headerView.findViewById(R.id.textView_user_mail);
        tv_usermail.setText(usuario.getMail());

        Bundle args = new Bundle();
        args.putInt("idusuario",usuario.getIdusuario());
        args.putString("nombre",usuario.getNombre());
        args.putString("mail",usuario.getMail());
        args.putString("pass",usuario.getPass());
        Fragment newFragment = new DashboardFragment();
        newFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.contenedor, newFragment);
        transaction.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getFragmentManager().getBackStackEntryCount() == 0){
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setMessage("¿Deseas salir de Overflow?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }else{
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.app_name);
            builder.setIcon(R.mipmap.ic_launcher);
            builder.setMessage("¿Deseas cerrar sesion en Overflow?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            new UsuarioDatabaseHelper(MainActivity.this).deleteUsuario(usuario);
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        int id = item.getItemId();
        if (id == R.id.nav_dashboard) {
            changeFragmentMain(new DashboardFragment());
        } else if (id == R.id.nav_activas) {
            changeFragmentMain(new ListFlujosFragment());
        } else if (id == R.id.nav_historico) {
            changeFragmentMain(new ListFlujos2Fragment());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeFragmentMain(Fragment newFragment){

        Bundle args = new Bundle();
        args.putInt("idusuario",usuario.getIdusuario());
        args.putString("nombre",usuario.getNombre());
        args.putString("mail",usuario.getMail());
        args.putString("pass",usuario.getPass());
        newFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.contenedor, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void changeFragmentAlternative(Fragment newFragment, Bundle bundle){

        bundle.putInt("idusuario",usuario.getIdusuario());
        bundle.putString("nombre",usuario.getNombre());
        bundle.putString("mail",usuario.getMail());
        bundle.putString("pass",usuario.getPass());
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.contenedor, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

}
