package com.xolidosoft.overflow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.xolidosoft.overflow.entities.Usuario;
import com.xolidosoft.overflow.helpers.UsuarioDatabaseHelper;
import com.xolidosoft.overflow.tools.MD5;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_MAIL = "EXTRA_MAIL";
    public static final String EXTRA_PASS = "EXTRA_PASS";

    private UserLoginTask mAuthTask = null;
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        List<Usuario> usuariosList = new UsuarioDatabaseHelper(this).getAllUsuarios();
        if(usuariosList!=null){
            for (Usuario u : usuariosList) {
                usuario = u;
            }
            if(usuario!=null) {
                showProgress(true);
                mAuthTask = new UserLoginTask(this, usuario.getMail(), usuario.getPass(), false);
                mAuthTask.execute((Void) null);
            }
        }

    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }
        mEmailView.setError(null);
        mPasswordView.setError(null);
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            mAuthTask = new UserLoginTask(this, email, password, true);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private Activity main_context;
        private boolean isNew;

        UserLoginTask(Activity activity, String email, String password, boolean isNew) {
            this.mEmail = email;
            this.mPassword = password;
            this.main_context = activity;
            this.isNew = isNew;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/loginData";
            String METHOD_NAME = "loginData";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass",MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                usuario = new Usuario();
                usuario.setIdusuario(Integer.parseInt(resultado.get(0).toString()));
                usuario.setNombre(resultado.get(1).toString());
                usuario.setMail(resultado.get(2).toString());
                usuario.setPass(mPassword);
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            if (success) {
                if(usuario.getIdusuario()==-1){
                    mEmailView.setError(getString(R.string.error_notexist_user));
                    mEmailView.requestFocus();
                }else{
                    if(usuario.getIdusuario()==0){
                        mEmailView.setError(getString(R.string.error_notrole_user));
                        mEmailView.requestFocus();
                    }else{
                        if(isNew) {
                            new UsuarioDatabaseHelper(main_context).insertUsuario(usuario);
                        }
                        Intent intent = new Intent(main_context, MainActivity.class);
                        intent.putExtra(EXTRA_ID, String.valueOf(usuario.getIdusuario()));
                        intent.putExtra(EXTRA_NAME, usuario.getNombre());
                        intent.putExtra(EXTRA_MAIL, usuario.getMail());
                        intent.putExtra(EXTRA_PASS, usuario.getPass());
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

