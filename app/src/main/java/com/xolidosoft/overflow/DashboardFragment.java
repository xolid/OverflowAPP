package com.xolidosoft.overflow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.xolidosoft.overflow.adapters.RecyclerViewAdapterTarea;
import com.xolidosoft.overflow.beans.Flujo;
import com.xolidosoft.overflow.beans.Tarea;
import com.xolidosoft.overflow.entities.Usuario;
import com.xolidosoft.overflow.tools.MD5;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashboardFragment extends Fragment {

    private Usuario usuario;
    private List<String[]> estados;
    private List<String[]> procesos;
    private List<String[]> tiempos;

    private View mDashboardProgressView;
    private View mDashbordFormView;

    private GetEstadoTask mLoadEstadosTask = null;
    private GetProcesoTask mLoadProcesosTask = null;
    private GetTiempoTask mLoadTiempoTask = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        usuario = new Usuario(args.getInt("idusuario"),args.getString("nombre"),args.getString("mail"),args.getString("pass"));
        return inflater.inflate(R.layout.dashboard_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        mDashbordFormView = getView().findViewById(R.id.dashboard_form);
        mDashboardProgressView = getView().findViewById(R.id.dashboard_progress);
    }

    public void onResume(){
        super.onResume();
        ((MainActivity)getActivity()).setActionBarTitle(getString(R.string.dashboard));
        showProgress(true);
        mLoadProcesosTask = new GetProcesoTask(getActivity(), usuario.getMail(), usuario.getPass());
        mLoadProcesosTask.execute((Void) null);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                mDashbordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                mDashbordFormView.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mDashbordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
                mDashboardProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mDashboardProgressView.animate().setDuration(shortAnimTime).alpha(
                        show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mDashboardProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                mDashboardProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mDashbordFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public class GetProcesoTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private Activity main_context;
        private PieChart pieChart;
        ArrayList<Integer> colors = new ArrayList();

        GetProcesoTask(Activity activity, String email, String password) {
            this.mEmail = email;
            this.mPassword = password;
            this.main_context = activity;
            for (int c : ColorTemplate.COLORFUL_COLORS)
                colors.add(c);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/getDataByProceso";
            String METHOD_NAME = "getDataByProceso";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass", MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                procesos = new ArrayList();
                try {
                    java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                    for(int i=0;i<resultado.size();i++){
                        Object property = resultado.get(i);
                        if(property instanceof SoapObject){
                            SoapObject final_object = (SoapObject) property;
                            String[] proceso = new String[2];
                            proceso[0] = String.valueOf(final_object.getProperty(0));
                            proceso[1] = String.valueOf(final_object.getProperty(1));
                            procesos.add(proceso);
                        }
                    }
                } catch (SoapFault soapFault) {
                } catch (IOException ioException) {
                } catch (Exception exception) {
                    try {
                        SoapObject property = (SoapObject) soapSerializationEnvelope.getResponse();
                        String[] proceso = new String[2];
                        proceso[0] = String.valueOf(property.getProperty(0));
                        proceso[1] = String.valueOf(property.getProperty(1));
                        procesos.add(proceso);
                    } catch (SoapFault e) { }
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLoadProcesosTask = null;
            showProgress(false);
            if (success) {
                pieChart = (PieChart) getView().findViewById(R.id.chart_proceso);
                pieChart.setRotationEnabled(true);
                pieChart.setHoleRadius(15f);
                pieChart.setCenterText("Activas");
                pieChart.setCenterTextSize(8);
                pieChart.setValueTextSize(10);
                pieChart.setUsePercentValues(true);
                pieChart.animateY(2000);
                pieChart.setTransparentCircleRadius(25);
                pieChart.setNoDataText("No hay datos de solicitudes activas");
                pieChart.setDescription("");
                addData();
                showProgress(true);
                mLoadEstadosTask = new GetEstadoTask(getActivity(), usuario.getMail(), usuario.getPass());
                mLoadEstadosTask.execute((Void) null);
            }
        }

        private void addData() {
            ArrayList<Entry> yVals = new ArrayList<Entry>();
            ArrayList<String> xVals = new ArrayList<String>();
            for (int i = 0; i < procesos.size(); i++){
                String[] proceso = procesos.get(i);
                xVals.add(proceso[0]);
                yVals.add(new Entry(Float.valueOf(proceso[1]), i));
            }
            PieDataSet dataSet = new PieDataSet(yVals, "Procesos");
            dataSet.setSliceSpace(3);
            dataSet.setSelectionShift(5);
            dataSet.setColors(colors);
            PieData data = new PieData(xVals, dataSet);
            pieChart.setData(data);
            pieChart.highlightValues(null);
            pieChart.invalidate();
        }

        @Override
        protected void onCancelled() {
            mLoadProcesosTask = null;
            showProgress(false);
        }
    }

    public class GetEstadoTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private Activity main_context;
        private PieChart pieChart;
        ArrayList<Integer> colors = new ArrayList();

        GetEstadoTask(Activity activity, String email, String password) {
            this.mEmail = email;
            this.mPassword = password;
            this.main_context = activity;
            for (int c : ColorTemplate.PASTEL_COLORS)
                colors.add(c);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/getDataByEstado";
            String METHOD_NAME = "getDataByEstado";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass", MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                estados = new ArrayList();
                try {
                    java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                    for(int i=0;i<resultado.size();i++){
                        Object property = resultado.get(i);
                        if(property instanceof SoapObject){
                            SoapObject final_object = (SoapObject) property;
                            String[] estado = new String[2];
                            estado[0] = String.valueOf(final_object.getProperty(0));
                            estado[1] = String.valueOf(final_object.getProperty(1));
                            estados.add(estado);
                        }
                    }
                } catch (SoapFault soapFault) {
                } catch (IOException ioException) {
                } catch (Exception exception) {
                    try {
                        SoapObject property = (SoapObject) soapSerializationEnvelope.getResponse();
                        String[] estado = new String[2];
                        estado[0] = String.valueOf(property.getProperty(0));
                        estado[1] = String.valueOf(property.getProperty(1));
                        estados.add(estado);
                    } catch (SoapFault e) { }
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLoadEstadosTask = null;
            showProgress(false);
            if (success) {
                pieChart = (PieChart) getView().findViewById(R.id.chart_estado);
                pieChart.setRotationEnabled(true);
                pieChart.setHoleRadius(15f);
                pieChart.setCenterText("Activas");
                pieChart.setCenterTextSize(8);
                pieChart.setValueTextSize(10);
                pieChart.setUsePercentValues(true);
                pieChart.animateY(2000);
                pieChart.setTransparentCircleRadius(25);
                pieChart.setNoDataText("No hay datos de solicitudes activas");
                pieChart.setDescription("");
                addData();
                showProgress(true);
                mLoadTiempoTask = new GetTiempoTask(getActivity(), usuario.getMail(), usuario.getPass());
                mLoadTiempoTask.execute((Void) null);
            }
        }

        private void addData() {
            ArrayList<Entry> yVals = new ArrayList<Entry>();
            ArrayList<String> xVals = new ArrayList<String>();
            for (int i = 0; i < estados.size(); i++){
                String[] estado = estados.get(i);
                xVals.add(estado[0]);
                yVals.add(new Entry(Float.valueOf(estado[1]), i));
            }
            PieDataSet dataSet = new PieDataSet(yVals, "Estados");
            dataSet.setSliceSpace(3);
            dataSet.setSelectionShift(5);
            dataSet.setColors(colors);
            PieData data = new PieData(xVals, dataSet);
            pieChart.setData(data);
            pieChart.highlightValues(null);
            pieChart.invalidate();
        }

        @Override
        protected void onCancelled() {
            mLoadEstadosTask = null;
            showProgress(false);
        }
    }

    public class GetTiempoTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private Activity main_context;
        private BarChart barChart;
        ArrayList<Integer> colors = new ArrayList();

        GetTiempoTask(Activity activity, String email, String password) {
            this.mEmail = email;
            this.mPassword = password;
            this.main_context = activity;
            for (int c : ColorTemplate.PASTEL_COLORS)
                colors.add(c);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/getDataByTiempo";
            String METHOD_NAME = "getDataByTiempo";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass", MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                tiempos = new ArrayList();
                try {
                    java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                    for(int i=0;i<resultado.size();i++){
                        Object property = resultado.get(i);
                        if(property instanceof SoapObject){
                            SoapObject final_object = (SoapObject) property;
                            String[] tiempo = new String[4];
                            tiempo[0] = String.valueOf(final_object.getProperty(0));
                            tiempo[1] = String.valueOf(final_object.getProperty(1));
                            tiempo[2] = String.valueOf(final_object.getProperty(2));
                            tiempo[3] = String.valueOf(final_object.getProperty(3));
                            tiempos.add(tiempo);
                        }
                    }
                } catch (SoapFault soapFault) {
                } catch (IOException ioException) {
                } catch (Exception exception) {
                    try {
                        SoapObject property = (SoapObject) soapSerializationEnvelope.getResponse();
                        String[] tiempo = new String[4];
                        tiempo[0] = String.valueOf(property.getProperty(0));
                        tiempo[1] = String.valueOf(property.getProperty(1));
                        tiempo[2] = String.valueOf(property.getProperty(2));
                        tiempo[3] = String.valueOf(property.getProperty(3));
                        tiempos.add(tiempo);
                    } catch (SoapFault e) { }
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLoadTiempoTask = null;
            showProgress(false);
            if (success) {
                barChart = (BarChart) getView().findViewById(R.id.chart_tiempo);
                barChart.setValueTextSize(10);
                barChart.animateY(2000);
                barChart.setNoDataText("No hay datos de solicitudes activas");
                barChart.setDescription("");
                addData();
            }
        }

        private void addData() {
            ArrayList<String> cabezera = new ArrayList();
            ArrayList<BarEntry> restante = new ArrayList();
            ArrayList<BarEntry> consumido = new ArrayList();
            for(int i=0;i<tiempos.size();i++){
                String[] tiempo = tiempos.get(i);
                cabezera.add("Folio: "+tiempo[0]);
                restante.add(new BarEntry(Integer.parseInt(tiempo[2]), i));
                consumido.add(new BarEntry(Integer.parseInt(tiempo[3]), i));
            }
            BarDataSet barDataSetRestante = new BarDataSet(restante,"Horas Restantes");
            barDataSetRestante.setColors(ColorTemplate.COLORFUL_COLORS);
            BarDataSet barDataSetConsumido = new BarDataSet(consumido,"Horas Consumidas");
            barDataSetConsumido.setColors(ColorTemplate.PASTEL_COLORS);
            ArrayList dataSets = new ArrayList<>();
            dataSets.add(barDataSetRestante);
            dataSets.add(barDataSetConsumido);
            BarData data = new BarData(cabezera,dataSets);
            barChart.setData(data);
            barChart.setDescription("");
            barChart.animateXY(2000, 2000);
            barChart.invalidate();
        }

        @Override
        protected void onCancelled() {
            mLoadTiempoTask = null;
            showProgress(false);
        }
    }

}
