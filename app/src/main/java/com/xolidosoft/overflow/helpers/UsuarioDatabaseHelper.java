package com.xolidosoft.overflow.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.xolidosoft.overflow.entities.Usuario;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "overflow_db";

    public UsuarioDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Usuario.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Usuario.TABLE_NAME);
        onCreate(db);
    }

    public long insertUsuario(int idusuario, String nombre, String mail, String pass) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Usuario.COLUMN_IDUSUARIO, idusuario);
        values.put(Usuario.COLUMN_NOMBRE, nombre);
        values.put(Usuario.COLUMN_MAIL, mail);
        values.put(Usuario.COLUMN_PASS, pass);
        long id = db.insert(Usuario.TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public long insertUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Usuario.COLUMN_IDUSUARIO, usuario.getIdusuario());
        values.put(Usuario.COLUMN_NOMBRE, usuario.getNombre());
        values.put(Usuario.COLUMN_MAIL, usuario.getMail());
        values.put(Usuario.COLUMN_PASS, usuario.getPass());
        long id = db.insert(Usuario.TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public Usuario getUsuario(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Usuario.TABLE_NAME,new String[]{Usuario.COLUMN_IDUSUARIO, Usuario.COLUMN_NOMBRE, Usuario.COLUMN_MAIL, Usuario.COLUMN_PASS},Usuario.COLUMN_IDUSUARIO + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Usuario usuario = new Usuario(
                cursor.getInt(cursor.getColumnIndex(Usuario.COLUMN_IDUSUARIO)),
                cursor.getString(cursor.getColumnIndex(Usuario.COLUMN_NOMBRE)),
                cursor.getString(cursor.getColumnIndex(Usuario.COLUMN_MAIL)),
                cursor.getString(cursor.getColumnIndex(Usuario.COLUMN_PASS))
        );
        cursor.close();
        return usuario;
    }

    public List<Usuario> getAllUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + Usuario.TABLE_NAME + " ORDER BY " +
                Usuario.COLUMN_NOMBRE + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Usuario usuario = new Usuario();
                usuario.setIdusuario(cursor.getInt(cursor.getColumnIndex(Usuario.COLUMN_IDUSUARIO)));
                usuario.setNombre(cursor.getString(cursor.getColumnIndex(Usuario.COLUMN_NOMBRE)));
                usuario.setMail(cursor.getString(cursor.getColumnIndex(Usuario.COLUMN_MAIL)));
                usuario.setPass(cursor.getString(cursor.getColumnIndex(Usuario.COLUMN_PASS)));
                usuarios.add(usuario);
            } while (cursor.moveToNext());
        }
        db.close();
        return usuarios;
    }

    public int getUsuariosCount() {
        String countQuery = "SELECT  * FROM " + Usuario.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int updateUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Usuario.COLUMN_IDUSUARIO, usuario.getIdusuario());
        values.put(Usuario.COLUMN_NOMBRE, usuario.getNombre());
        values.put(Usuario.COLUMN_MAIL, usuario.getMail());
        values.put(Usuario.COLUMN_PASS, usuario.getPass());
        return db.update(Usuario.TABLE_NAME, values, Usuario.COLUMN_IDUSUARIO + " = ?", new String[]{String.valueOf(usuario.getIdusuario())});
    }

    public void deleteUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Usuario.TABLE_NAME, Usuario.COLUMN_IDUSUARIO + " = ?", new String[]{String.valueOf(usuario.getIdusuario())});
        db.close();
    }

    public void deleteAllUsuario() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ Usuario.TABLE_NAME);
        db.close();
    }
}