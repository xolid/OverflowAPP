package com.xolidosoft.overflow.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xolidosoft.overflow.R;
import com.xolidosoft.overflow.beans.Comentario;
import com.xolidosoft.overflow.beans.Tarea;

import java.util.Date;
import java.util.List;

public class RecyclerViewAdapterComentario extends RecyclerView.Adapter<RecyclerViewAdapterComentario.MyViewHolder> {

    public List<Comentario> comentarios;
    private Activity main_context;

    public RecyclerViewAdapterComentario(List<Comentario> comentarios, Activity activity){
        this.comentarios = comentarios;
        this.main_context = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_comentario, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Comentario comentario = (Comentario) comentarios.get(position);
        holder.tvcomentario_usuario.setText(comentario.getUsuario());
        holder.tvcomentario_fecha.setText(comentario.getFecha());
        holder.tvcomentario_texto.setText(comentario.getTexto());

    }


    @Override
    public int getItemCount() {
        return comentarios.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvcomentario_usuario;
        private TextView tvcomentario_fecha;
        private TextView tvcomentario_texto;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvcomentario_usuario = (TextView)itemView.findViewById(R.id.tv_comentario_usuario);
            tvcomentario_fecha = (TextView)itemView.findViewById(R.id.tv_comentario_fecha);
            tvcomentario_texto = (TextView)itemView.findViewById(R.id.tv_comentario_texto);
        }
    }
}
