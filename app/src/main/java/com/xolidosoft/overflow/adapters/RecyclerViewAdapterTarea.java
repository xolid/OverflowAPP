package com.xolidosoft.overflow.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xolidosoft.overflow.R;
import com.xolidosoft.overflow.beans.Tarea;

import java.util.Date;
import java.util.List;

public class RecyclerViewAdapterTarea extends RecyclerView.Adapter<RecyclerViewAdapterTarea.MyViewHolder> {

    public List<Tarea> tareas;
    private Activity main_context;

    public RecyclerViewAdapterTarea(List<Tarea> tareas, Activity activity){
        this.tareas = tareas;
        this.main_context = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_tarea, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Tarea tarea = (Tarea)tareas.get(position);
        holder.tvtarea_actividad.setText(tarea.getActividad());
        holder.tvtarea_usuario.setText(tarea.getUsuario());
        holder.tvtarea_frecibido.setText(tarea.getFrecibido());
        holder.tvtarea_fasignado.setText(tarea.getFasignado());

        holder.tvtarea_fproceso.setText(tarea.getFproceso());
        if(tarea.getEstado().equals("EN PROGRESO") || tarea.getEstado().equals("RECHAZADA") || tarea.getEstado().equals("RECIBIDA") || tarea.getEstado().equals("PENDIENTE")) {
            try {
                Date proceso = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tarea.getFproceso());
                if (proceso.after(new Date())) {
                    holder.tvtarea_fproceso.setTextColor(Color.GREEN);
                } else {
                    holder.tvtarea_fproceso.setTextColor(Color.RED);
                }
            } catch (Exception e) { }
        }

        holder.tvtarea_fliberado.setText(tarea.getFliberado());
        holder.tvtarea_estado.setText(tarea.getEstado());
        if(tarea.getEstado().equals("PENDIENTE")){
            holder.tvtarea_estado.setTextColor(Color.MAGENTA);
        }else{
            if(tarea.getEstado().equals("RECIBIDA")){
                holder.tvtarea_estado.setTextColor(Color.CYAN);
            }else{
                if(tarea.getEstado().equals("RECHAZADA")){
                    holder.tvtarea_estado.setTextColor(Color.RED);
                }else{
                    if(tarea.getEstado().equals("EN PROGRESO")){
                        holder.tvtarea_estado.setTextColor(Color.BLUE);
                    }else{
                        if(tarea.getEstado().equals("COMPLETADA")){
                            holder.tvtarea_estado.setTextColor(Color.GREEN);
                        }else {
                            if (tarea.getEstado().equals("DECLINADA")) {
                                holder.tvtarea_estado.setTextColor(Color.RED);
                            } else {
                                if (tarea.getEstado().equals("APROBADA")) {
                                    holder.tvtarea_estado.setTextColor(Color.GREEN);
                                } else {
                                    if (tarea.getEstado().equals("CANCELADA")) {
                                        holder.tvtarea_estado.setTextColor(Color.DKGRAY);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }


    @Override
    public int getItemCount() {
        return tareas.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvtarea_actividad;
        private TextView tvtarea_usuario;
        private TextView tvtarea_estado;
        private TextView tvtarea_frecibido;
        private TextView tvtarea_fasignado;
        private TextView tvtarea_fproceso;
        private TextView tvtarea_fliberado;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvtarea_actividad = (TextView)itemView.findViewById(R.id.tv_tarea_actividad);
            tvtarea_usuario = (TextView)itemView.findViewById(R.id.tv_tarea_usuario);
            tvtarea_estado = (TextView)itemView.findViewById(R.id.tv_tarea_estado);
            tvtarea_frecibido = (TextView)itemView.findViewById(R.id.tv_tarea_frecibido);
            tvtarea_fasignado = (TextView)itemView.findViewById(R.id.tv_tarea_fasignado);
            tvtarea_fproceso = (TextView)itemView.findViewById(R.id.tv_tarea_fproceso);
            tvtarea_fliberado = (TextView)itemView.findViewById(R.id.tv_tarea_fliberado);
        }
    }
}
