package com.xolidosoft.overflow.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xolidosoft.overflow.R;
import com.xolidosoft.overflow.beans.Comentario;
import com.xolidosoft.overflow.beans.Registro;

import java.util.List;

public class RecyclerViewAdapterRegistro extends RecyclerView.Adapter<RecyclerViewAdapterRegistro.MyViewHolder> {

    public List<Registro> registros;
    private Activity main_context;

    public RecyclerViewAdapterRegistro(List<Registro> registros, Activity activity){
        this.registros = registros;
        this.main_context = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_registro, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Registro registro = (Registro) registros.get(position);
        holder.tvregistro_dispara.setText(registro.getDispara());
        holder.tvregistro_fecha.setText(registro.getFecha());
        holder.tvregistro_objetivo.setText(registro.getObjetivo());
        holder.tvregistro_accion.setText(registro.getAccion());

    }


    @Override
    public int getItemCount() {
        return registros.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvregistro_dispara;
        private TextView tvregistro_fecha;
        private TextView tvregistro_objetivo;
        private TextView tvregistro_accion;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvregistro_dispara = (TextView)itemView.findViewById(R.id.tv_registro_dispara);
            tvregistro_fecha = (TextView)itemView.findViewById(R.id.tv_registro_fecha);
            tvregistro_objetivo = (TextView)itemView.findViewById(R.id.tv_registro_objetivo);
            tvregistro_accion = (TextView)itemView.findViewById(R.id.tv_registro_accion);
        }
    }
}
