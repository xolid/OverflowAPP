package com.xolidosoft.overflow.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.xolidosoft.overflow.ComentarioFragment;
import com.xolidosoft.overflow.RegistroFragment;
import com.xolidosoft.overflow.TareaFragment;
import com.xolidosoft.overflow.MainActivity;
import com.xolidosoft.overflow.R;
import com.xolidosoft.overflow.beans.Flujo;

import java.util.Date;
import java.util.List;

public class RecyclerViewAdapterFlujo extends RecyclerView.Adapter<RecyclerViewAdapterFlujo.MyViewHolder> {

    public List<Flujo> flujos;
    private Activity main_context;

    public RecyclerViewAdapterFlujo(List<Flujo> flujos, Activity activity){
        this.flujos = flujos;
        this.main_context = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_flujo, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Flujo flujo = (Flujo)flujos.get(position);
        holder.tvfolio.setText("Folio: "+flujo.getIdflujo());
        holder.tvnombre.setText(flujo.getNombre());
        holder.tvfcreado.setText(flujo.getFcreado());
        holder.tvfrequerido.setText(flujo.getFrequerido());
        if(flujo.getEstado().equals("EN PROGRESO") || flujo.getEstado().equals("RECHAZADA") || flujo.getEstado().equals("RECIBIDA") || flujo.getEstado().equals("PENDIENTE")) {
            try {
                Date requerido = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(flujo.getFrequerido());
                if (requerido.after(new Date())) {
                    holder.tvfrequerido.setTextColor(Color.GREEN);
                } else {
                    holder.tvfrequerido.setTextColor(Color.RED);
                }
            } catch (Exception e) { }
        }
        holder.tvdescripcion.setText(flujo.getDescripcion());
        holder.tvestado.setText(flujo.getEstado());
        if(flujo.getEstado().equals("PENDIENTE")){
            holder.tvestado.setTextColor(Color.MAGENTA);
        }else{
            if(flujo.getEstado().equals("RECIBIDA")){
                holder.tvestado.setTextColor(Color.CYAN);
            }else{
                if(flujo.getEstado().equals("RECHAZADA")){
                    holder.tvestado.setTextColor(Color.RED);
                }else{
                    if(flujo.getEstado().equals("EN PROGRESO")){
                        holder.tvestado.setTextColor(Color.BLUE);
                    }else{
                        if(flujo.getEstado().equals("COMPLETADA")){
                            holder.tvestado.setTextColor(Color.GREEN);
                        }else{
                            if(flujo.getEstado().equals("DECLINADA")){
                                holder.tvestado.setTextColor(Color.RED);
                            }else{
                                if(flujo.getEstado().equals("APROBADA")){
                                    holder.tvestado.setTextColor(Color.GREEN);
                                }else{
                                    if(flujo.getEstado().equals("CANCELADA")){
                                        holder.tvestado.setTextColor(Color.DKGRAY);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        holder.bttareas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("idflujo",Integer.parseInt(flujo.getIdflujo()));
                ((MainActivity)main_context).changeFragmentAlternative(new TareaFragment(),bundle);
            }
        });

        holder.btcomentarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("idflujo",Integer.parseInt(flujo.getIdflujo()));
                ((MainActivity)main_context).changeFragmentAlternative(new ComentarioFragment(),bundle);
            }
        });

        holder.btregistros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("idflujo",Integer.parseInt(flujo.getIdflujo()));
                ((MainActivity)main_context).changeFragmentAlternative(new RegistroFragment(),bundle);
            }
        });

    }


    @Override
    public int getItemCount() {
        return flujos.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvfolio;
        private TextView tvnombre;
        private TextView tvfcreado;
        private TextView tvfrequerido;
        private TextView tvdescripcion;
        private TextView tvestado;
        private Button bttareas;
        private Button btcomentarios;
        private Button btregistros;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvfolio = (TextView)itemView.findViewById(R.id.tv_folio);
            tvnombre = (TextView)itemView.findViewById(R.id.tv_nombre);
            tvfcreado = (TextView)itemView.findViewById(R.id.tv_fcreado);
            tvfrequerido = (TextView)itemView.findViewById(R.id.tv_frequerido);
            tvdescripcion = (TextView)itemView.findViewById(R.id.tv_descipcion);
            tvestado = (TextView)itemView.findViewById(R.id.tv_estado);
            bttareas = (Button)itemView.findViewById(R.id.bt_tareas);
            btcomentarios = (Button)itemView.findViewById(R.id.bt_comentarios);
            btregistros = (Button)itemView.findViewById(R.id.bt_registros);
        }
    }
}
