package com.xolidosoft.overflow.entities;

public class Usuario {

    public static final String TABLE_NAME = "usuarios";
    public static final String COLUMN_IDUSUARIO = "idusuario";
    public static final String COLUMN_NOMBRE = "nombre";
    public static final String COLUMN_MAIL = "mail";
    public static final String COLUMN_PASS = "pass";

    private int idusuario;
    private String nombre;
    private String mail;
    private String pass;

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_IDUSUARIO + " INTEGER PRIMARY KEY,"
                    + COLUMN_NOMBRE + " TEXT,"
                    + COLUMN_MAIL + " TEXT,"
                    + COLUMN_PASS + " TEXT"
                    + ")";

    public Usuario() {
    }

    public Usuario(int idusuario, String nombre, String mail, String pass) {
        this.idusuario = idusuario;
        this.nombre = nombre;
        this.mail = mail;
        this.pass = pass;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


}