package com.xolidosoft.overflow.beans;

public class Flujo {

    private String idflujo;
    private String proceso;
    private String usuario;
    private String nombre;
    private String descripcion;
    private String fcreado;
    private String frequerido;
    private String fsolicitado;
    private String fentregado;
    private String fterminado;
    private String estado;

    public Flujo() {
    }

    public Flujo(String proceso, String usuario, String nombre, String descripcion, String fcreado, String frequerido, String estado) {
        this.proceso = proceso;
        this.usuario = usuario;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fcreado = fcreado;
        this.frequerido = frequerido;
        this.estado = estado;
    }
    public Flujo(String proceso, String usuario, String nombre, String descripcion, String fcreado, String frequerido, String fsolicitado, String fentregado, String fterminado, String estado) {
        this.proceso = proceso;
        this.usuario = usuario;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fcreado = fcreado;
        this.frequerido = frequerido;
        this.fsolicitado = fsolicitado;
        this.fentregado = fentregado;
        this.fterminado = fterminado;
        this.estado = estado;
    }

    public String getIdflujo() {
        return this.idflujo;
    }

    public void setIdflujo(String idflujo) {
        this.idflujo = idflujo;
    }
    public String getProceso() {
        return this.proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }
    public String getUsuario() {
        return this.usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getFcreado() {
        return this.fcreado;
    }

    public void setFcreado(String fcreado) {
        this.fcreado = fcreado;
    }
    public String getFrequerido() {
        return this.frequerido;
    }

    public void setFrequerido(String frequerido) {
        this.frequerido = frequerido;
    }
    public String getFsolicitado() {
        return this.fsolicitado;
    }

    public void setFsolicitado(String fsolicitado) {
        this.fsolicitado = fsolicitado;
    }
    public String getFentregado() {
        return this.fentregado;
    }

    public void setFentregado(String fentregado) {
        this.fentregado = fentregado;
    }
    public String getFterminado() {
        return this.fterminado;
    }

    public void setFterminado(String fterminado) {
        this.fterminado = fterminado;
    }
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}