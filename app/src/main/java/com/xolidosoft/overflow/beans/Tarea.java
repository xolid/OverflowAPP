package com.xolidosoft.overflow.beans;

public class Tarea {

    private String idtarea;
    private String actividad;
    private String flujo;
    private String usuario;
    private String frecibido;
    private String fasignado;
    private String fproceso;
    private String fliberado;
    private String estado;

    public String getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(String idtarea) {
        this.idtarea = idtarea;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getFlujo() {
        return flujo;
    }

    public void setFlujo(String flujo) {
        this.flujo = flujo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFrecibido() {
        return frecibido;
    }

    public void setFrecibido(String frecibido) {
        this.frecibido = frecibido;
    }

    public String getFasignado() {
        return fasignado;
    }

    public void setFasignado(String fasignado) {
        this.fasignado = fasignado;
    }

    public String getFproceso() {
        return fproceso;
    }

    public void setFproceso(String fproceso) {
        this.fproceso = fproceso;
    }

    public String getFliberado() {
        return fliberado;
    }

    public void setFliberado(String fliberado) {
        this.fliberado = fliberado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
