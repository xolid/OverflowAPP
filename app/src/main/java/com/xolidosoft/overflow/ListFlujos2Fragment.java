package com.xolidosoft.overflow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xolidosoft.overflow.adapters.RecyclerViewAdapterFlujo;
import com.xolidosoft.overflow.beans.Flujo;
import com.xolidosoft.overflow.entities.Usuario;
import com.xolidosoft.overflow.tools.MD5;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListFlujos2Fragment extends Fragment {

    private View mProgressView;
    private View mLoadFormView;

    private GetFlujosListTask mLoadTask = null;
    private List<Flujo> flujos;
    private Usuario usuario;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        usuario = new Usuario(args.getInt("idusuario"),args.getString("nombre"),args.getString("mail"),args.getString("pass"));
        return inflater.inflate(R.layout.list_flujos_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        mLoadFormView = getView().findViewById(R.id.load_form);
        mProgressView = getView().findViewById(R.id.load_progress);
    }

    public void onResume(){
        super.onResume();
        ((MainActivity)getActivity()).setActionBarTitle(getString(R.string.fragment_list_closed_title));
        showProgress(true);
        mLoadTask = new GetFlujosListTask(getActivity(), usuario.getMail(), usuario.getPass());
        mLoadTask.execute((Void) null);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                mLoadFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                mLoadFormView.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoadFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mProgressView.animate().setDuration(shortAnimTime).alpha(
                        show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mLoadFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public class GetFlujosListTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private Activity main_context;

        GetFlujosListTask(Activity activity, String email, String password) {
            this.mEmail = email;
            this.mPassword = password;
            this.main_context = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/getAllByUsuarioCLOSED";
            String METHOD_NAME = "getAllByUsuarioCLOSED";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass", MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                flujos = new ArrayList();
                try {
                    java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                    for(int i=0;i<resultado.size();i++){
                        Object property = resultado.get(i);
                        if(property instanceof SoapObject){
                            SoapObject final_object = (SoapObject) property;
                            Flujo flujo = new Flujo();
                            flujo.setIdflujo(String.valueOf(final_object.getProperty(0)));
                            flujo.setProceso(String.valueOf(final_object.getProperty(1)));
                            flujo.setUsuario(String.valueOf(final_object.getProperty(2)));
                            flujo.setNombre(String.valueOf(final_object.getProperty(3)));
                            flujo.setDescripcion(String.valueOf(final_object.getProperty(4)));
                            flujo.setFcreado(String.valueOf(final_object.getProperty(5)));
                            flujo.setFrequerido(String.valueOf(final_object.getProperty(6)));
                            flujo.setFsolicitado(String.valueOf(final_object.getProperty(7)));
                            flujo.setFentregado(String.valueOf(final_object.getProperty(8)));
                            flujo.setFterminado(String.valueOf(final_object.getProperty(9)));
                            flujo.setEstado(String.valueOf(final_object.getProperty(10)));
                            flujos.add(flujo);
                        }
                    }
                } catch (SoapFault soapFault) {
                } catch (IOException ioException) {
                } catch (Exception exception) {
                    try {
                        SoapObject property = (SoapObject) soapSerializationEnvelope.getResponse();
                        Flujo flujo = new Flujo();
                        flujo.setIdflujo(String.valueOf(property.getProperty(0)));
                        flujo.setProceso(String.valueOf(property.getProperty(1)));
                        flujo.setUsuario(String.valueOf(property.getProperty(2)));
                        flujo.setNombre(String.valueOf(property.getProperty(3)));
                        flujo.setDescripcion(String.valueOf(property.getProperty(4)));
                        flujo.setFcreado(String.valueOf(property.getProperty(5)));
                        flujo.setFrequerido(String.valueOf(property.getProperty(6)));
                        flujo.setFsolicitado(String.valueOf(property.getProperty(7)));
                        flujo.setFentregado(String.valueOf(property.getProperty(8)));
                        flujo.setFterminado(String.valueOf(property.getProperty(9)));
                        flujo.setEstado(String.valueOf(property.getProperty(10)));
                        flujos.add(flujo);
                    } catch (SoapFault e) { }
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLoadTask = null;
            showProgress(false);
            if (success) {
                RecyclerViewAdapterFlujo adapter = new RecyclerViewAdapterFlujo(flujos,main_context);
                RecyclerView myView =  (RecyclerView)getActivity().findViewById(R.id.recyclerview);
                myView.setHasFixedSize(true);
                myView.setAdapter(adapter);
                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                myView.setLayoutManager(llm);
            }
        }

        @Override
        protected void onCancelled() {
            mLoadTask = null;
            showProgress(false);
        }
    }

}
