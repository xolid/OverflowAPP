package com.xolidosoft.overflow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xolidosoft.overflow.adapters.RecyclerViewAdapterTarea;
import com.xolidosoft.overflow.beans.Flujo;
import com.xolidosoft.overflow.beans.Tarea;
import com.xolidosoft.overflow.entities.Usuario;
import com.xolidosoft.overflow.tools.MD5;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TareaFragment extends Fragment {

    private View mProgressView;
    private View mLoadFormView;

    private GetFlujoTask mLoadTask = null;
    private GetTareaTask mDetailTask = null;

    private List<Tarea> tareas;
    private Usuario usuario;
    private Flujo flujo;
    private int idflujo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        usuario = new Usuario(args.getInt("idusuario"),args.getString("nombre"),args.getString("mail"),args.getString("pass"));
        idflujo = args.getInt("idflujo");
        return inflater.inflate(R.layout.tarea_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        mLoadFormView = getView().findViewById(R.id.load_form);
        mProgressView = getView().findViewById(R.id.load_progress);
    }

    public void onResume(){
        super.onResume();
        ((MainActivity)getActivity()).setActionBarTitle(getString(R.string.fragment_tarea_title));
        showProgress(true);
        mLoadTask = new GetFlujoTask(getActivity(), usuario.getMail(), usuario.getPass(), idflujo);
        mLoadTask.execute((Void) null);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                mLoadFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                mLoadFormView.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoadFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mProgressView.animate().setDuration(shortAnimTime).alpha(
                        show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mLoadFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public class GetFlujoTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private final int mID;
        private Activity main_context;

        GetFlujoTask(Activity activity, String email, String password, int id) {
            this.mEmail = email;
            this.mPassword = password;
            this.mID = id;
            this.main_context = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/getFlujoByUsuario";
            String METHOD_NAME = "getFlujoByUsuario";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("idflujo",mID);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass", MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                flujo = new Flujo();
                flujo.setIdflujo(resultado.get(0).toString());
                flujo.setProceso(resultado.get(1).toString());
                flujo.setUsuario(resultado.get(2).toString());
                flujo.setNombre(resultado.get(3).toString());
                flujo.setDescripcion(resultado.get(4).toString());
                flujo.setFcreado(resultado.get(5).toString());
                flujo.setFrequerido(resultado.get(6).toString());
                flujo.setFsolicitado(resultado.get(7).toString().equals("anyType{}")?"N/D":resultado.get(7).toString());
                flujo.setFentregado(resultado.get(8).toString().equals("anyType{}")?"N/D":resultado.get(8).toString());
                flujo.setFterminado(resultado.get(9).toString().equals("anyType{}")?"N/D":resultado.get(9).toString());
                flujo.setEstado(resultado.get(10).toString());
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLoadTask = null;
            showProgress(false);
            if (success) {
                ((TextView)main_context.findViewById(R.id.tv_flujo_nombre)).setText(flujo.getNombre());
                ((TextView)main_context.findViewById(R.id.tv_flujo_descripcion)).setText(flujo.getDescripcion());
                ((TextView)main_context.findViewById(R.id.tv_flujo_proceso)).setText(flujo.getProceso());
                ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setText(flujo.getEstado());
                if(flujo.getEstado().equals("PENDIENTE")){
                    ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.MAGENTA);
                }else{
                    if(flujo.getEstado().equals("RECIBIDA")){
                        ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.CYAN);
                    }else{
                        if(flujo.getEstado().equals("RECHAZADA")){
                            ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.RED);
                        }else{
                            if(flujo.getEstado().equals("EN PROGRESO")){
                                ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.BLUE);
                            }else{
                                if(flujo.getEstado().equals("COMPLETADA")){
                                    ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.GREEN);
                                }else{
                                    if(flujo.getEstado().equals("DECLINADA")){
                                        ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.RED);
                                    }else{
                                        if(flujo.getEstado().equals("APROBADA")){
                                            ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.GREEN);
                                        }else{
                                            if(flujo.getEstado().equals("CANCELADA")){
                                                ((TextView)main_context.findViewById(R.id.tv_flujo_estado)).setTextColor(Color.DKGRAY);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ((TextView)main_context.findViewById(R.id.tv_flujo_fcreado)).setText(flujo.getFcreado());
                ((TextView)main_context.findViewById(R.id.tv_flujo_frequerido)).setText(flujo.getFrequerido());
                if(flujo.getEstado().equals("EN PROGRESO") || flujo.getEstado().equals("RECHAZADA") || flujo.getEstado().equals("RECIBIDA") || flujo.getEstado().equals("PENDIENTE")) {
                    try {
                        Date requerido = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(flujo.getFrequerido());
                        if (requerido.after(new Date())) {
                            ((TextView)main_context.findViewById(R.id.tv_flujo_frequerido)).setTextColor(Color.GREEN);
                        } else {
                            ((TextView)main_context.findViewById(R.id.tv_flujo_frequerido)).setTextColor(Color.RED);
                        }
                    } catch (Exception e) { }
                }
                ((TextView)main_context.findViewById(R.id.tv_flujo_fsolicitado)).setText(flujo.getFsolicitado());
                ((TextView)main_context.findViewById(R.id.tv_flujo_fentregado)).setText(flujo.getFentregado());
                ((TextView)main_context.findViewById(R.id.tv_flujo_fterminado)).setText(flujo.getFterminado());
                //cargar tareas
                showProgress(true);
                mDetailTask = new GetTareaTask(getActivity(), usuario.getMail(), usuario.getPass(), idflujo);
                mDetailTask.execute((Void) null);
            }
        }

        @Override
        protected void onCancelled() {
            mLoadTask = null;
            showProgress(false);
        }
    }

    public class GetTareaTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private final int mID;
        private Activity main_context;

        GetTareaTask(Activity activity, String email, String password, int id) {
            this.mEmail = email;
            this.mPassword = password;
            this.mID = id;
            this.main_context = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String SOAP_ACTION = "http://webservices/getAllTareasByFlujo";
            String METHOD_NAME = "getAllTareasByFlujo";
            String NAMESPACE = "http://webservices/";
            String URL = "http://189.206.75.114:8080/OverflowWS/WSRedactar";
            try{
                SoapObject soapObject = new SoapObject(NAMESPACE,METHOD_NAME);
                soapObject.addProperty("idflujo",mID);
                soapObject.addProperty("mail",mEmail);
                soapObject.addProperty("pass", MD5.getMD5(mPassword));
                SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapSerializationEnvelope.setOutputSoapObject(soapObject);
                HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
                httpTransportSE.call(SOAP_ACTION,soapSerializationEnvelope);
                tareas = new ArrayList();
                try {
                    java.util.Vector resultado = (java.util.Vector) soapSerializationEnvelope.getResponse();
                    for(int i=0;i<resultado.size();i++){
                        Object property = resultado.get(i);
                        if(property instanceof SoapObject){
                            SoapObject final_object = (SoapObject) property;
                            Tarea tarea = new Tarea();
                            tarea.setIdtarea(String.valueOf(final_object.getProperty(0)));
                            tarea.setActividad(String.valueOf(final_object.getProperty(1)));
                            tarea.setFlujo(String.valueOf(final_object.getProperty(2)));
                            tarea.setUsuario(String.valueOf(final_object.getProperty(3)));
                            tarea.setFrecibido(String.valueOf(final_object.getProperty(4)).equals("anyType{}")?"N/D":String.valueOf(final_object.getProperty(4)));
                            tarea.setFasignado(String.valueOf(final_object.getProperty(5)).equals("anyType{}")?"N/D":String.valueOf(final_object.getProperty(5)));
                            tarea.setFproceso(String.valueOf(final_object.getProperty(6)).equals("anyType{}")?"N/D":String.valueOf(final_object.getProperty(6)));
                            tarea.setFliberado(String.valueOf(final_object.getProperty(7)).equals("anyType{}")?"N/D":String.valueOf(final_object.getProperty(7)));
                            tarea.setEstado(String.valueOf(final_object.getProperty(8)));
                            tareas.add(tarea);
                        }
                    }
                } catch (SoapFault soapFault) {
                } catch (IOException ioException) {
                } catch (Exception exception) {
                    try {
                        SoapObject property = (SoapObject) soapSerializationEnvelope.getResponse();
                        Tarea tarea = new Tarea();
                        tarea.setIdtarea(String.valueOf(property.getProperty(0)));
                        tarea.setActividad(String.valueOf(property.getProperty(1)));
                        tarea.setFlujo(String.valueOf(property.getProperty(2)));
                        tarea.setUsuario(String.valueOf(property.getProperty(3)));
                        tarea.setFrecibido(String.valueOf(property.getProperty(4)).equals("anyType{}")?"N/D":String.valueOf(property.getProperty(4)));
                        tarea.setFasignado(String.valueOf(property.getProperty(5)).equals("anyType{}")?"N/D":String.valueOf(property.getProperty(5)));
                        tarea.setFproceso(String.valueOf(property.getProperty(6)).equals("anyType{}")?"N/D":String.valueOf(property.getProperty(6)));
                        tarea.setFliberado(String.valueOf(property.getProperty(7)).equals("anyType{}")?"N/D":String.valueOf(property.getProperty(7)));
                        tarea.setEstado(String.valueOf(property.getProperty(8)));
                        tareas.add(tarea);
                    } catch (SoapFault e) { }
                }
                return true;
            }catch(Exception e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mDetailTask = null;
            showProgress(false);
            if (success) {
                RecyclerViewAdapterTarea adapter = new RecyclerViewAdapterTarea(tareas,main_context);
                RecyclerView myView =  (RecyclerView)getActivity().findViewById(R.id.recyclerViewTareas);
                myView.setHasFixedSize(true);
                myView.setAdapter(adapter);
                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                myView.setLayoutManager(llm);
                //alimentando avance
                int total_tareas = 0;
                int total_completas = 0;
                for(Tarea tarea:tareas){
                    total_tareas++;
                    if(!tarea.getFliberado().equals("N/D")){
                        total_completas++;
                    }
                }
                double porcentaje = 0;
                if(total_tareas>0){
                    porcentaje = (total_completas/total_tareas)*100.0;
                }
                if(porcentaje==100){
                    ((TextView)main_context.findViewById(R.id.tv_flujo_avance)).setTextColor(Color.GREEN);
                    ((ProgressBar)main_context.findViewById(R.id.pb_flujo_avance)).getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                }
                if(porcentaje>66.66&&porcentaje<100){
                    ((TextView)main_context.findViewById(R.id.tv_flujo_avance)).setTextColor(Color.YELLOW);
                    ((ProgressBar)main_context.findViewById(R.id.pb_flujo_avance)).getProgressDrawable().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
                }
                if(porcentaje<66.66){
                    ((TextView)main_context.findViewById(R.id.tv_flujo_avance)).setTextColor(Color.RED);
                    ((ProgressBar)main_context.findViewById(R.id.pb_flujo_avance)).getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                }
                ((TextView)main_context.findViewById(R.id.tv_flujo_avance)).setText(total_completas+" / "+total_tareas);
                ((ProgressBar)main_context.findViewById(R.id.pb_flujo_avance)).setProgress(new Double(porcentaje).intValue());
            }
        }

        @Override
        protected void onCancelled() {
            mDetailTask = null;
            showProgress(false);
        }
    }

}
